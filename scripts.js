document.addEventListener("DOMContentLoaded", function () {
  // window.location.search - ?en-custine

  var query = window.location.search ? window.location.search.replace("?", "") : "-";
  var querySplit = query.split("&")[0].split("-");
  var lang = querySplit[0];
  var currentPage = querySplit[1];

  var langs = ["en", "uk"];
  if (!langs.includes(lang)) {
    lang = "en";
  }

  var about = resolveLang(ABOUT);
  var ussr = resolveLang(USSR);
  var noblewoman = resolveLang(NOBLEWOMAN);
  var signature = resolveLang(SIGNATURE);
  var miscStrings = resolveLang(MISC_STRINGS);
  var authors = AUTHORS.map(resolveLang);

  pages = ['about', 'ussr', 'noblewoman'].concat(authors.map(a => a.author))
  if (!pages.includes(currentPage)) {
    currentPage = undefined;
  }
  
  var currentAuthor = authors.find(a => a.author === currentPage) || undefined;

  var quotes = QUOTES.map(addQuoteSlug).map(resolveLang).map(resolveQuoteSignature).filter(filterByAuthor);

  if (currentPage) {
    miscStrings.otherLanguageLinkUrl += "-" + currentPage;
  }
  if (currentPage === "ussr") {
    miscStrings.otherLanguageLinkUrl = undefined;
  }
  if (currentPage === "noblewoman") {
    miscStrings.otherLanguageLinkUrl = undefined;
  }

  var templateVars = {};
  templateVars.miscStrings = miscStrings;
  templateVars.quotes = quotes;
  templateVars.authors = authors;
  templateVars.currentAuthor = currentAuthor;
  templateVars.about = (currentPage === 'about' ? about : undefined);
  templateVars.ussr = (currentPage === 'ussr' ? ussr : undefined);
  templateVars.noblewoman = (currentPage === 'noblewoman' ? noblewoman : undefined);
  templateVars.lang = lang;
  templateVars.mainPage = !currentPage;
  templateVars.notMainPage = !!currentPage;
  templateVars.notAboutPage = currentPage !== 'about';
  templateVars.notAuthorPage = (currentAuthor === undefined);
  templateVars.uk = lang === 'uk';
  templateVars.en = lang === 'en';

  var templateElement = document.getElementById("template");
  var template = templateElement.innerHTML;
  templateElement.innerHTML = Mustache.render(template, templateVars);
  templateElement.style.display = 'block';
  var tooltipTimeoutHandle = undefined;

  function resolveLang(object) {
    Object.keys(object).forEach(key => {
      if (key.startsWith(lang + ".")) {
        var langlessKey = key.replace(lang + ".", "");
        object[langlessKey] = object[key];
      }
    });
    return object;
  }

  function resolveQuoteSignature(quote) {
    quote.signature = signature[quote.author];
    quote.signatureYear = signature[quote.author + ".year"];
    return quote;
  };

  function filterByAuthor(quote) {
    return !currentPage || quote.author === currentPage;
  }

  function addQuoteSlug(quote) {
    const textStart = quote.bubble.split(' ').slice(0, 3).join('-').toLowerCase().replace(/[^a-z-]/g, '');
    quote.slug = `${quote.author}-${textStart}`;
    return quote;
  };

  document.querySelectorAll('a[data-copy-link]').forEach(function (link) {
    link.addEventListener('click', event => {
      event.preventDefault(); // Prevent default navigation behavior
      const slug = event.currentTarget.getAttribute('data-copy-link');
      const currentURLWithoutFragment = window.location.href.split('#')[0];
      navigator.clipboard.writeText(currentURLWithoutFragment + '#' + slug);

      const tooltip = event.currentTarget.querySelector('.copy-link-tooltip');
      setTimeout(function () {
        tooltip.classList.remove("active");
        setTimeout(function () {
          tooltip.classList.add("active");
        }, 10);
      }, 10);
      if (tooltipTimeoutHandle !== undefined) {
        clearTimeout(tooltipTimeoutHandle);
      }
    });
  });
});