window.QUOTES = [
     {
      "author": "custine",
      "bubble": "Russia is mostly destined to punish the corrupt civilization of Europe, by the agency of a new invasion.",
      "uk.bubble": "Основне призначення росії — покарати кволу європейську цивилізацію за допомогою нового нашестя.",
      "paragraphs": [
        "It appears to me that Russia is mostly destined to punish the corrupt civilization of Europe, by the agency of a new invasion. The eternal tyranny of the East threatens us constantly; and we shall have to bow to it, if our extravagance and immorality render us worthy of the punishment.",
      ],
      "uk.paragraphs": [
        "На мою думку, основне призначення росії — покарати кволу європейську цивилізацію за допомогою нового нашестя. Нам невпинно загрожує одвічна східна тиранія, і ми станемо її жертвами, коли накличемо на себе цю кару своїм безпуттям та криводушністю.",
      ],
      "pic": "pics/germanyshame.png",
      "uk.pic": "pics/germanyganba.png",
    },
    {
      "author": "custine",
      "bubble": "A man is obliged to mistreat those below him.",
      "uk.bubble": "Людину зобов’язують зневажати інших",
      "paragraphs": [
        "A man, as soon as he rises above the common level, acquires the right, and, furthermore, is obliged to mistreat those below him. It is his duty to transmit the punches that he receives from those above him.",
        "The spirit of cruelty descend from stage to stage down to the foundations of this unhappy society, which exists only by violence — a violence so great, that it forces the slave to falsify himself by thanking his tyrant."
      ],
      "uk.paragraphs": [
        "Ледве вибившись з бруду, людина одразу здобуває право, більше того, її зобов’язують, зневажати інших і давати їм стусани, які сиплються на неї зверху. Росіянин заподіює зло, щоб винагородити себе за утиски, яких зазнає сам.",
        "Так дух беззаконня спускається донизу суспільними сходами зі сходинки на сходинку і до самих основ пронизує це нещасне суспільство, що грунтується лише на примусі — примусі настільки жорстокому, що змушує раба обманювати самого себе й дякувати тирану."
      ],
      "pic": "pics/mistreat.png"
    },
    {
      "author": "delamarre",
      "bubble": "Conquerors appropriated the name of the enslaved people as their own",
      "uk.bubble": "Загарбники привласнили собі ім’я поневоленого народу",
      "uk.paragraphs": [
       "Рутенці [українці], народ, який проживає між Московією та Польщею, що раніше звався руси (russes) або русини (russiens), був поневолений у минулому столітті московитами.",
       "Загарбники привласнили собі ім’я поневоленого народу, щоб одержати уявне право на його володіння. Саме тому руси й московити сьогодні вважаються рівнозначними назвами, хоча насправді вони цілковито різні.",
       "Ця зумисна плутанина надала московитам змогу привласнити і саму історію русів.",
     ],
     "paragraphs": [
      "Living between Muscovy and Poland, Ruthenians [Ukrainians], who previously were also called Russes or Rusyns (Russiens), became enslaved by the Muscovites in the last century.",
      "Conquerors appropriated the name of the enslaved people as their own, above all in order to receive the imaginary ownership of them. It is necessary to understand that the word Russes and Muscovites, which today seem to us synonymous, are in fact quite different.",
      "This deliberate confusion allowed Muscovites to absorb even the history of the Ruthenians.",
      ],
   },
    {
      "author": "kostomarov",
      "bubble": " The most common popular nickname for a Ukrainian peasant in the upper society is “khokhol-fool”; in the manor houses, even parrots are taught this phrase.",
      "uk.bubble": "Найпоширеніше назвисько південноруського селянина у вищому суспільстві було “хохол-дурак”; у панських маєтках цієї фрази навіть навчали папуг.",
      "paragraphs": [
        "South Russian [Ukrainian] people have a reason for being especially reluctant to accept education in a language, different from their own. This nationality, with its native language, have been in contempt and disdain, due to prejudices dominating in society. The ordinary South Russian people are regarded as a generation of fools. The most common popular nickname for a South Russian peasant in the upper society is “khokhol-fool”; in the manor houses, even parrots are taught this phrase. Thus, the South Russian nationality and enlightenment seem as opposite and incompatible forces.",
        "Those who originated from these people, at the first contact with education, or rather with pitiful semi-education, not only tried to get rid of the signs of their nationality but were even ashamed of their origin; sometimes they hid it and pretended — mostly unsuccessfully and laughably — to be Russians, and generally looked down on the people they themselves came from. Who did not mock this nationality? Who did not spit on it? Who did not throw dirt, and sometimes stones, at it? ",
        "Like the landowner, afraid that his children might accidentally utter, in front of guests, stupid peasant words heard in the maid's or servant's room;",
        "Like the court officer, complaining about the stupidity of the khokhols and convinced of the superiority of Russians;",
        "Like the soldier, ready at the slightest relaxation of discipline to consider the ox, chicken, and the wife of the peasant, where he is billeted, his property;",
        "Like the Russian trader, who does not consider it a sin to cheat the stupid khokhol and who invented the saying: “khokhols are no people; no milk for three weeks for khokhol customer!”.",
        "Even Belinsky, for all his liberalism, ridiculed it, seeing as clownish the attempts of some South Russians to promote the native language of their people, daring to acknowledge their own unity with these people, at the risk of being considered mad or malicious. It is not surprising that with the general stamping of this people as fools, the imprint of apathy and mental timidity fell upon them.",
      ],
      "uk.paragraphs": [
        "Південноруський народ має всі підстави вкрай неохоче навчатися чужою для нього мовою. Ця народність, з її рідною мовою, безупинно зазнавала зневаги та ганьби через упередження, що панують у суспільстві. Простий південноруський  народ вважався якимось поколінням дурнів; найпоширеніше назвисько південноруського селянина у вищому суспільстві було “хохол-дурак”; у панських маєтках цієї фрази навіть навчали папуг.",
        "Через це малоросійська народність і освіта здавалися протилежними та несумісними стихіями. Ті, хто походив з цього народу, при першому зіткненні з освіченістю, а скоріше із жалюгідною напівосвіченістю, не лише намагалися позбутися ознак своєї народності, але навіть соромилися свого походження; іноді приховували його і видавали себе — здебільшого безуспішно і сміховинно — за Великоросів, і загалом цуралися народу, з якого самі ж і вийшли. Хто не насміхався над цією народністю? Хто не плював у неї? Хто не кидався у неї брудом, а іноді й камінням?",
        "Це й поміщик, занепокоєний, щоб його діти не бовкнули, при гостях, дурних, селянських слів, почутих у дівочій або лакейській;",
        "І становий офіцер, який скаржився на дурних хохлів, переконаний у вищости Великоросів;",
        "І солдат, який від найменшого послаблення дисципліни готовий привласнити вола, курок, і жінку селянина, у  якого він розквартирований;",
        "І великоруський купець, який не вважав за гріх ошукати дурного хохла і вигадав приказку: “хохли — не люди; за хохла три п'ятниці молока не їсти!”",
        "Над нею знущався навіть Бєлінський, який з усім своїм лібералізмом не побачив нічого, окрім шутовського перевдягання із фрака у свитку, у прагненні деяких малоросів підняти рідну мову свого народу, у зухвалості визнати свою власну єдність з цим народом, ризикуючи здатися божевільними або зловмисними.",
      ],
    },
     {
      "author": "clarke",
      "bubble": "North is mistakenly considered the civilised part of the country.",
      "uk.bubble": "Північна частина країни помилково вважається цивілізованою.",
      "paragraphs": [
        "Approaching the southern part of the empire, the strong characteristics of the Russian people are less frequently observed. Happily for the traveller, in proportion as his distance is increased from that which has been mistakenly considered the civilised part of the country, he has less to complain of theft, of fraud, and of dissimulation.",
        "In the more northern provinces, he is cautioned to beware of the inhabitants of the Ukraine, and the Cossacks. The chambers of our inn were immediately over the town jail, and it is quite unnecessary to add of what nation its tenants were composed.",
        "The Russian finds it dangerous to travel in the Ukraine, and along the Don, because he is conscious that the inhabitants of these countries know too well with whom they have to deal.",
      ],
      "uk.paragraphs": [
        "З наближенням до південної частини імперії особливости характеру російського народу трапляються все рідше. На щастя для мандрівника, що далі він перебуває від тієї частини країни, яка помилково вважається цивілізованою, то менше він скаржиться на крадіжки, шахрайство та непристойність.",
        "У північних провінціях попереджають мандрівників, щоб остерігалися мешканців України та козаків, але ми жили поряд з міською в’язницею, і не варто й казати, з якої нації походила більшість її мешканців.",
        "Росіянин вважає небезпечним подорожувати Україною та вздовж Дону, бо він усвідомлює, що мешканці цих країн добре знають, з ким вони мають справу.",
      ],
    },
    {
      "author": "dauteroche",
      "bubble": "European manners have gained very little ground in Russia.",
      "uk.bubble": "Європейські манери, які спробували запровадити в Росії, не надто тут прижилися.",
      "paragraphs": [
        "European manners, however, have gained very little ground in Russia; because they are not conformable to the despotism of the government. They have nevertheless introduced luxury, and brought on a communication between Russians and foreigners; which has only contributed to make the Russians more unhappy, by giving them opportunity of comparing their state of slavery with that of a free people.",
      ],
      "uk.paragraphs": [
        "Європейські манери, які спробували запровадити в Росії, не надто тут прижилися, оскільки вони не сумісні з деспотизмом влади. Проте вони принесли з собою розкоші та можливість спілкуватися з іноземцями. Втім, це лише зробило росіян нещаснішими, бо дозволило їм зіставити своє рабське життя з життям вільних людей.",
      ],
    },
    {
      "author": "custine",
      "bubble": "Here it is too easy to be deceived by the appearances of civilization.",
      "uk.bubble": "Тут дуже легко обманутися видимістю цивілізації.",
      "paragraphs": [
        "Here it is too easy to be deceived by the appearances of civilization. If you look at the court and people in it, you may suppose yourself among a nation far advanced in social culture and political economy.",
        "But when you reflect on the relations which exist between the different classes of society, when you observe how small is the number of these classes — finally, when you examine attentively the groundwork of manners and of things, you perceive the existence of a real barbarian, scarcely disguised under revolting magnificence.",
      ],
      "uk.paragraphs": [
        "Тут дуже легко обманутися видимістю цивілізації. Коли ви перебуваєте при дворі, то вам може здатися, ніби ви потрапили до країни, високорозвиненої як в культурному, так і в економічному й політичному сенсі.",
        "Але якщо дослідити взаємини між соціальними класами цього суспільства, якщо звернути увагу на мізерну кількість тих класів, зрештою, якщо уважно придивитися до звичаїв та вчинків, ви помітите справжнє варварство, ледь прикрите обурливою пишнотою.",
      ],
    },
    {
      "author": "clarke",
      "bubble": "If the Archipelago should fall under the dominion of Russia, the fine remains of ancient Greece will be no more.",
      "uk.bubble": "Якби Грецький архіпелаг опинився під владою Росії, від його античних памʼяток не зосталося б і сліду.",
      "paragraphs": [
        "When they [Russians] settled in the country, the remains of the city of Chersonesus were so considerable, that all its gates were standing. These they soon demolished; and, proceeding in their favorite employment of laying waste, they pulled down, broke, buried, and destroyed whatever they could find which might serve to illustrate its former history; blowing up its ancient foundations; tearing open tombs; overthrowing temples; and then, removing the masses of stone and marble to Aktiar [Sevastopol], exposed them for sale, by cubic measure, to serve as materials in building.",
        "If the Archipelago should fall under the dominion of Russia, the fine remains of ancient Greece will be no more; Athens will be razed, and not a stone be left to mark where the city stood.",
      ],
      "uk.paragraphs": [
        "Коли росіяни утвердилися в країні, руїни Херсонесу були в такому доброму стані, що всі його брами стояли і досі. Їх усіх вони невдовзі зруйнували; і, взявшись до свого улюбленго плюндрування, вони трощили, ламали, закопували та нищили все, що свідчило про його історичне минуле; підривали давні фундаменти, розкопували гробниці, руйнували храми, а затим перевозили камінь та мармур до Ахтіяру [Севастополь], де продавали їх на вагу як будівельний матеріял.",
        "Якби Грецький архіпелаг опинився під владою Росії, від його античних памʼяток не зосталося б і сліду; Афіни було б стерто вщент, і каменя на камені не лишилося би там, де колись стояло місто.",
      ],
    "pic": "pics/crimea-platformorg.png",
    "uk.pic": "pics/crimea-platformorguk.png",

    },
    {
      "author": "custine",
      "bubble": "I was astonished to see the disinterested admiration of Emperor’s people for the luxuries which they do not possess.",
      "uk.bubble": "Я був вражений безкорисливим захопленням, з яким народ дивиться на багатства, які йому не належать.",
      "paragraphs": [
          "I was astonished to see the disinterested admiration of Emperor’s people for the luxuries which they do not possess, nor ever will, and which they do not dare even to regret. Difficult to believe that despotism could make so many disinterested philosophers.",
      ],
      "uk.paragraphs": [
        "Я був вражений безкорисливим захопленням, з яким народ дивиться на багатства, які йому не належать, яких у нього ніколи не буде і яким він навіть не наважується заздрити. Важко повірити, що деспотизм міг породити стільки апатичних філософів.",
      ],
    "pic": "pics/disinterestedphilosophersen.png",
    "uk.pic": "pics/disinterestedphilosophersuk.png",
    },
     {
      "author": "custine",
      "bubble": "People are as indifferent to honesty in action as to truth in speech.",
      "uk.bubble": "Народ байдужий як до справедливости у діях, так і до правди у словах.",
      "paragraphs": [
        "The country swarms with the most adroit and audacious of robbers. Their crimes are so frequent that justice does not dare to be strict. Everything here is done unpredictably or by exception. A peculiar system, which too well accords with the ill-regulated minds of the people, who are as indifferent to honesty in action as to truth in speech.",
      ],
      "uk.paragraphs": [
        "Країна аж кишить спритними й нахабними крадіями. Злочинів тут скоюється так багато, що правосуддя не наважується бути суворим до злочинців. Та й взагалі, все тут робиться не за правилом, а за примхою. Ця примхлива система, на жаль, дуже пасує до безладних уявлень народу, байдужого як до справедливости у діях, так і до правди у словах.",
      ],
    },
    {
      "author": "clarke",
      "bubble": "I would have rather dined of the floor of Ukrainian house than on the table of any Russian prince.",
      "uk.bubble": "Я більше хотів би обідати на підлозі української хати, ніж за столом будь-якого російського князя.",
      "paragraphs": [
        "We proceeded from Paulovskoy to Kazinskoy Chutor, a village inhabited by Malo-Russians [Ukrainians] and Russians mingled together.",
        "The distinction between the two people might be made without the smallest inquiry, from the striking contrast between filth and cleanliness.",
        "In the stable of the posthouse horses were kept with a degree of order and neatness which would have done credit to any nobleman's stud in Britain. The house of the poor superintendent villager was equally admirable; everything appeared clean and decent. There was no litter, nor was anything out of its place.",
        "It was quite a new thing to us, to hesitate whether we should clean our boots before walking into an apartment, on the floor of which I would rather have dined than on the table of any Russian prince."
      ],
      "uk.paragraphs": [
        "Ми виїхали з Павловської до Казинського Хутора, селища, населеного одночасно малоросіянами та росіянами.",
        "Різниця між цими народами видна без найменшого дослідження - через разючий контраст чистоти і бруду.",
        "Те, як трималися коні в конюшні, дало б фору конюшні будь-якого вельможі Британії. Будинок бідного голови села був чудовий, всяка річ була чистою та пристойною, ніде не було сміття.",
        "Все було настільки чисте, що вперше ми вагалися, чи слід чистити наші чоботи, перш ніж входити до хати, на підлозі якої я більше хотів би обідати, ніж за столом будь-якого російського князя."
      ],
      "pic": "pics/houses.jpg"
    },
     {
      "author": "clyman",
      "bubble": "When I realized that it was plain ordinary bread she wanted, I could hardly believe my ears.",
      "uk.bubble": "Коли я збагнула, що йдеться про звичайний хліб, то не повірила власним вухам.",
      "paragraphs": [
        "At last I found a woman who could speak a little Russian. I asked her where I could get a quart of milk and ten eggs.",
        "She eyed me curiously for a moment, then demanded: “You want them for money?”",
        "“Of course,” I answered, “I don't expect to get them for nothing.”",
        "“You don’t understand,” she said. “We don’t sell eggs for money or milk. We want bread. Have you any?”",
        "At first I thought she meant grain, for in Russian and Ukrainian the word is the same. But when I realized that it was plain ordinary bread she wanted, I could hardly believe my ears! In Russia the villages were forlorn, the peasants ragged and dirty, they grumbled about not having enough food, but they did not ask for bread. If they had anything to sell, they were quite content to let it go for money.",
        "Here in the Ukraine, where the peasants seemed so clean and everything well-kept, they wanted bread!",
      ],
      "uk.paragraphs": [
        "Зрештою мені вдалося знайти жінку, яка трохи говорила російською. Я запитала її, де можна купити літр молока та десяток яєць. Вона глянула на мене допитливо, потім запитала: “Ви хочете купити їх за гроші?”",
        "“Звісно, — відповіла я, — я не очікую отримати щось задарма”.",
        "“Ви не розумієте”, — сказала вона. “Ми не продаємо яйця за гроші, так само молоко. Ми хочемо хліба. У вас він є?”",
        "Спочатку я думала, що вона має на увазі зерно, оскільки в російській та українській це слово має ще й таке значення. Але коли я збагнула, що йдеться про звичайний хліб, то не повірила власним вухам! У Росії були занедбані села, селяни ходили в лахмітті, брудні, скаржилися на брак їжі, однак не просили хліба. Якщо вони мали щось для продажу, то залюбки віддавали продукти за гроші.",
        "Тут, в Україні, де селяни були такими охайними, і все було доглянутим, вони хотіли хліба!",
      ],
    },
     {
      "author": "steinbeck",
      "bubble": "In Kyiv people laughed in the streets.",
      "uk.bubble": "У Києві на вулицях лунає сміх.",
      "paragraphs": [
        "Although Kyiv is greatly destroyed while Moscow is not, the people in Kyiv did not seem to have the dead weariness of the Moscow people. They did not slouch when they walked, their shoulders were back, and they laughed in the streets. Of course this might be local, for the Ukrainians are not like the Russian; they are a separate species of Slav.",
      ],
      "uk.paragraphs": [
        "Хоча Київ, на відміну від Москви, зруйнований майже дощенту, місцеві жителі поводяться інакше від змарнілих, похмурих москвитян. Вони не ходять згорблячись, плечі в них розправлені, ба більше, на вулицях лунає сміх. Безперечно, це може бути особливістю самих українців, адже вони відрізняються від росіян, це окремий слов'янський народ.",
      ],
    },
    {
      "author": "custine",
      "bubble": "In what other country a man of the lower orders would assist in the infliction of an arbitrary punishment upon one of his companions?",
      "uk.bubble": "Спробуйте в будь-якій іншій країні знайти простолюдина, який допомагатиме у розправі над його товаришем, якого карають через чиєсь свавілля!",
      "paragraphs": [
        "I have seen how some courier of some minister dragged from his seat a young coachman, and never ceased striking him until he had covered his face with blood. The victim submitted to the torture like a real lamb, without the least resistance, and in the same manner as one would yield to some inevitable commotion of nature. The passerby were in no degree moved or excited by the cruelty.",
        "And one of the comrades of the sufferer, who was watering his horses a few steps off, obedient to a sign of the enraged feldjager, approached to hold his horse's bridle during the time that he was pleased to prolong the punishment. In what other country a man of the lower orders would assist in the infliction of an arbitrary punishment upon one of his companions?",
        "The scene in question took place in the finest part of the city, and at the busiest hour. When the unfortunate man was released, he wiped away the blood, which streamed down his cheeks, remounted his seat, and recommenced his bows and salutations as usual.",
        "It should be recollected that this abomination was enacted in the midst of a silent crowd. A people governed in a Christian manner would protest against a social discipline which destroys all individual liberty."
      ],
      "uk.paragraphs": [
        "На моїх очах кур'єр якогось міністра стягнув з козла молодого кучера і перестав бити його, лише коли побачив, що обличчя у того геть залите кров’ю. Жертва цієї екзекуції витримала її з воістину янгольським терпінням, без найменшого опору, ніби поступаючись якійсь природній негоді. Перехожих анітрохи не бентежила така жорстокість.",
        "А один з товаришів потерпілого, який напував своїх коней неподалік, за сигналом розгніваного фельд’єгеря підбіг, аби тримати віжки упряжки цього держслужбовця, доки той не зволив завершити екзекуцію. Спробуйте в будь-якій іншій країні знайти простолюдина, який допомагатиме у розправі над його товаришем, якого карають через чиєсь свавілля!",
        "Описана мною сцена відбувалася в найкрасивішому кварталі міста, серед великої кількости людей. Коли нещасного нарешті відпустили, він витер кров, що струміла щоками, спокійнісінько сів назад на козли й знову пустився відважувати поклони при кожній новій зустрічі.",
        "Необхідно зауважити, що відбувалося це все в присутности мовчазного натовпу. Нація, якою керують по-християнськи, обурилася б такими покараннями, що нищать будь-яку особисту свободу."
      ],
      "pic": "pics/russianprotest.jpg",
    },
     {
      "author": "custine",
      "bubble": "It is deemed less inconvenient to carry on the mistake than to publish it.",
      "uk.bubble": "Краще лишити все як є, аніж визнати помилку.",
      "paragraphs": [
        "The punishment of death ”does not exist” in this land except for the crime of high treason. There are certain criminals whom they nevertheless kill. When a criminal is condemned to more than a hundred strokes of the knout, the executioner, who understands the meaning of such a sentence, kills him through humanity, by mortally striking him at the third blow. And yet the punishment of death is ”abolished”!",
        "In a Russian state prison no one even remembers crimes of some of the prisoners. They are therefore retained forever, because it is not known who should take care of it. And it is deemed less inconvenient to carry on the mistake than to publish it — the bad effect of such a delayed justice is feared. We are unceasingly told that there is no punishment of death in Russia. To bury alive, then, is not to kill?!",
      ],
      "uk.paragraphs": [
        "Смертна кара в цій країні скасована для всіх злочинів, крім державної зради. Однак деяких злочинців вбивають. Коли злочинця засуджують до сотні ударів батогом, кат, який розуміє, що означає такий вирок, третім ударом з людяности вбиває нещасного. Але смертну кару ”скасовано”!",
        "Про деяких злочинців невідомо навіть, який злочин вони скоїли. Тож їх тримають під вартою довіку, тому що ніхто не знає, що з ними робити. Усі вважають, що краще лишити все як є, аніж визнати помилку, непокоючись поганих наслідків запізного правосуддя. Але нам на кожному кроці твердять, що смертну кару скасовано. Хіба поховати людину заживо не означає її вбити?",
      ],
      "pic": "pics/asplanned.png",
      "uk.pic": "pics/poplanu.png",
    },
    {
      "author": "custine",
      "bubble": "Among the voices that relate these things to the glory of the Emperor, not one separates itself from the universal chorus to protest against such autocratic miracles.",
      "uk.bubble": "Серед голосів, які вбачають у тих задумах велич імператора, не знайдеться жодного, який вибився б із загального хору і заступився за нещасних.",
      "paragraphs": [
        "Every Russian is proud of being able to say to us, \"You take three years to debate on the means of rebuilding a theatre, whilst our Emperor raises again, in one year, the largest palace in the universe.\" And this childish triumph does not appear to them too dearly bought by the death of a few thousand wretched workers, sacrificed to that sovereign impatience, that imperial fantasy, which constitutes the national glory.",
        "Whilst I, a Frenchman, see nothing but inhumane vanity in this achievement, not a single protestation is raised from one end of this immense empire to the other against the orgies of absolute power.",
        "People and government are here in unison. A man who brought up in the idolatry of self, a man who has an unlimited power over sixty millions of men (or at least of beings that resemble men), should not undertake to put an end to such a state of things — this does not surprise me.",
        "The wonder is, that among the voices that relate these things to the glory of this individual, not one separates itself from the universal chorus to protest in favor of humanity against such autocratic miracles. It may be said of the Russians, great and small, that they are drunk with slavery."
      ],
      "uk.paragraphs": [
        "Усі росіяни пишаються можливістю сказати чужинцям: „Ось бачите, ви три роки сперечаєтеся про те, як перебудувати театральну залу; а наш імператор за один рік підняв з руїн найбільший палац світу“. І вони вважають, що загибель кількох тисяч робітників, принесених у жертву найвищому нетерпінню та примсі імператора, яку видають за потребу нації — жалюгідна дрібниця і зовсім не дорога ціна за цей дитячий захват.",
        "Я, француз, бачу в усьому цьому саме лише нелюдське марнославство, однак серед цієї неосяжної імперії не знайшлося нікого, хто б висловив невдоволення цим збоченим всевладдям.",
        "Тут народ і уряд єдині. Мене ж найбільше дивує не те, що людина, з дитинства привчена поклонятися самій собі, людина, яку шістдесят мільйонів людей (або ж, у всякому разі, створінь, які нагадують людей) називають всемогутньою, замислює і доводить до кінця подібні задуми.",
        "Мене дивує, що серед голосів, які вбачають у цих задумах велич тієї людини, не знайдеться жодного, який вибився б із загального хору і заступився за нещасних, які заплатили життям за самодержавні чудеса. Про всіх росіян, хоч би до якого стану вони належали, можна сказати, що вони впиваються своїм рабством."
      ],
      "pic": "pics/winterpalace.jpg"
    },
    {
      "author": "steinbeck",
      "bubble": "The Russians are taught that their government is good and that their job is to back it up in all ways.",
      "uk.bubble": "Росіян навчають, що їхній уряд — якнайкращий, і їхнє завдання — підтримувати його з усіх боків.",
      "paragraphs": [
        "One of the deepest divisions between the Russians and the Americans or British is in their feeling toward their governments. The Russians are taught, and trained, and encouraged to believe that their government is good, that every part of it is good, and that their job is to carry it forward, to back it up in all ways.",
      ],
      "uk.paragraphs": [
        "Одна із найбільших розбіжностей між росіянами та американцями (чи британцями) полягає в їхніх почуттях до свого уряду. Росіян навчають, і тренують, і спонукають вірити, що їхній уряд — якнайкращий, і що кожна його складова теж дуже хороша, і що їхнє завдання — допомагати йому і підтримувати з усіх боків.",
      ],
    },
    {
      "author": "custine",
      "bubble": "Among the Russians, the word of a man is nothing better than the false key of a robber.",
      "uk.bubble": "У росіян слово — не що інше, як злодійська відмичка.",
      "paragraphs": [
        "In Latin lands, a promise is a sacred thing — a pledge to the giver as well as the receiver. Among the Russians, the word of a man is nothing better than the false key of a robber — it serves to break into the interests of others."
      ],
      "uk.paragraphs": [
        "У латинських країнах обіцянка — священна річ, а слово є запорукою, яку цінує однаково і той, хто дає обіцянку, і той, кому її дають. У росіян слово — не що інше, як злодійська відмичка для проникнення в чуже житло."
      ],
      "pic": "pics/putincrimea.png",
      "uk.pic": "pics/putincrimeauk.png"
    },
    {
      "author": "custine",
      "bubble": "A man may attain the first military dignity without having served in any army.",
      "uk.bubble": "Людина може вшановуватися найвищими військовими почестями попри цілковиту відсутність досвіду військової служби.",
      "paragraphs": [
        "The will of the Emperor is the sole means by which an individual is promoted, so that a man, rising step by step to the highest rank in this artificial nation, may attain the first military dignity without having served in any army.",
      ],
      "uk.paragraphs": [
        "Просування кожної окремої людини в чині залежить виключно від волі імператора. Тож людина, підіймаючись сходинка за сходинкою до найвищого звання в цій штучно влаштованій нації, може вшановуватися першорядними військовими почестями попри цілковиту відсутність досвіду військової служби.",
      ],
       "pic": "pics/shoigu.png",
       "uk.pic": "pics/shoiguuk.png",
    },
    {
      "author": "clarke",
      "bubble": "Who would trust to leave a carriage among the Russians?",
      "uk.bubble": "Xто наважився б довірити транспортування карети росіянам?",
      "paragraphs": [
        "Concerning the inhabitants of the country called Malo-Russia, a French gentleman, who had long resided among them, assured me he used neither locks to his doors nor to his coffers; and among the Cossacks, as in Sweden, trunk may be sent open, for a distance of 500 miles, without risking the loss of any of its contents.",
        "Mr Rowan, banker of Moscow, was convinced, by the breaking down of his carriage, to abandon it in the midst of the territory of the Don Cossacks, and it was afterwards brought safe to him at Taganrock, with all its accessories and contents, by the unsolicited and selfless labour of that people. Who would trust to leave a carriage, or even a trunk, although encased, doubly locked, and directed, among the Russians?",
      ],
      "uk.paragraphs": [
        "Щодо мешканців країни під назвою Малоросія, французький джентльмен, який протягом тривалого часу проживав у цій країні, запевняв нас, що він не замикає замком двері власного будинку. І так само, як у Швеції, може відправити власний багаж без замків на відстань п’ятисот миль, не ризикуючи втратити будь-що з його вмісту.",
        "Містера Рована, банкіра із Москви, переконали залишити несправну карету посеред земель Донських козаків, і згодом карету в цілости й схоронности привезли до нього у Таганрог, з усім її вмістом та приладдям, завдяки безкорисливій праці тих людей. А хто наважився б лишити карету, та навіть і скриню, хай би й двічі замкнену, і довірити її транспортування росіянам?",
      ],
    },
     {
      "author": "custine",
      "bubble": "The dreams of vanity and the love of command.",
      "uk.bubble": "Марнославні мрії та жага до панування над іншими.",
      "paragraphs": [
        "I can see no compensation for the misery of being born under this system, except the dreams of vanity and the love of command. On these passions I stumble every time I analyze the moral life of the inhabitants of Russia.",
      ],
      "uk.paragraphs": [
        "Я не бачу іншої винагороди за нещастя народитися за такого режиму, крім марнославних мрій та жаги до панування над іншими. Щоразу, коли я намагаюся осягнути моральне життя людей, які живуть у росії, я знову і знову помічаю ці пристрасті.",
      ],
    },
    {
      "author": "custine",
      "bubble": "What is not dared to be spoken of among the principals, is yet more carefully avoided by subordinates.",
      "uk.bubble": "Якщо про щось не наважується розмовляти керівництво, про це тим паче не говорять підлеглі.",
      "paragraphs": [
        "Two Englishmen sailed to Peterhoff, and three hours afterwards they perished, together with several women, children and men, who were in the same boat.",
        "What numberless discussions would this have given rise to in any other land except this! How many newspapers would’ve said, and how many voices would’ve repeated, that the police never does its duty, boats were not seaworthy, that authorities increase the danger by indifference or corruption!",
        "Nothing of the kind here. Two lines in the Gazette, without details, is all the information publicly given. And at court, in the city, in the saloons of fashion, not a word is spoken. The petty employees are more timid than the great lords. What is not dared to be spoken of among the principals, is yet more carefully avoided by subordinates.",
      ],
      "uk.paragraphs": [
        "Двоє англійців сіли у човен й відплили до Петергофа, й трьома годинами пізніше обидва загинули, разом із жінками, дітьми й чоловіками, які були на тому ж судні.",
        "Уявіть, скільки б дискусій спричинила би така подія в будь-якій країні, окрім цієї! Скільки газет написали б, і скільки голосів би це повторювало, що поліція не виконує своїх обов‘язків, човни ненадійні, а влада посилює небезпеку своєю байдужістю чи скорумпованістю.",
        "Тут — нічого. Два рядки в газеті без жодних подробиць — ось і вся публічна інформація. Ані при дворі, ані в місті, ані у великосвітських вітальнях — ні слова. Дрібні чиновники боязкіші за вельмож. Якщо про щось не наважується розмовляти керівництво, про це тим паче не говорять підлеглі.",
      ],
       "pic": "pics/nopanic.jpg",
      "uk.pic": "pics/panikinema.jpg"
    },
     {
      "author": "custine",
      "bubble": "Oppressor escapes ridicule by the terror and mystery he surrounds himself with.",
      "uk.bubble": "Для гнобителя єдиний спосіб уникнути глузувань — наводити страх і зберігати таємничість.",
      "paragraphs": [
        "In Russia, power, unlimited as it is, entertains an extreme dread of disapproval, or even of free speech. An oppressor is of all others the man who most fears the truth: he only escapes ridicule by the terror and mystery he surrounds himself with.",
      ],
      "uk.paragraphs": [
        "Хоч би якою безмежною була влада російських монархів, вони вкрай бояться несхвалення й навіть простої відвертості. З усіх людей понад усе боїться правди гнобитель: для нього єдиний спосіб уникнути глузувань — наводити страх і зберігати таємничість.",
      ],
      "pic": "pics/notbluffing.png",
      "uk.pic": "pics/neblef.png",
    },
     {
      "author": "custine",
      "bubble": "The impression which their country may make on the minds of travellers occupies their thoughts incessantly.",
      "uk.bubble": "Вони тільки й думають що про враження, яке справить їхня країна на стороннього спостерігача.",
      "paragraphs": [
        "I am much struck by the extreme vulnerability of the Russians as regards the judgment which strangers may form about them. The impression which their country may make on the minds of travellers occupies their thoughts incessantly. It seems to me as though the Russians would be happy to become even yet worse and more barbarous than they are, provided they were thought better and more civilized.",
      ],
       "uk.paragraphs": [
        "Мене вражає непомірна тривога росіян щодо того, як про них подумає чужинець. Вони тільки й думають що про враження, яке справить їхня країна на стороннього спостерігача. Мені здається, що вони б з радістю стали ще злішими та дикішими, ніж вони є, аби лише їх вважали добрішими та цивілізованішими.",
      ],
      },
     {
      "author": "istoriarusiv",
      "bubble": "Nasty custom of giving all other nations despiteful nicknames.",
      "uk.bubble": "Мали якийсь паскудний звичай давати всім народам презирливі прізвиська.",
      "paragraphs": [
        "Those Cossacks, being on campaigns together with Russian archers and marksmen, endured frequent and offensive taunts from those soldiers for shaving their heads. Those soldiers, who were then wearing grey clothes and bast shoes, unshaven and with beards, that is, having peasant appearance, had, however, an unexpectedly high opinion of themselves and had some nasty custom of giving all other nations despiteful nicknames, such as: Pollacks, [similar diminishing slur for Tatars], and so on.",
      ],
      "uk.paragraphs": [
        "Козаки тії, будучи в походах разом із стрільцями та сагайдачниками російськими, терпіли од тих солдатів часті та дошкульні глузування за те, що голять свої голови. Солдати тії, бувши ще тоді в сірячинах та в личаних постолах, неголені і в бородах, себто в усій мужичій образині, були, одначе, про себе незрозуміле високої думки і мали якийсь паскудний звичай давати всім народам презирливі прізвиська, як от: Полячишки, Татаришки і так далі.",
      ],
    },
     {
      "author": "robinson",
      "bubble": "All non-Russians are considered inferior.",
      "uk.bubble": "Усі неросіяни вважаються неповноцінними.",
      "paragraphs": [
        "All non-Russians are considered inferior. On the unofficial scale of inferiority, the Armenians, Georgians, and Ukrainians are more acceptable than other non-Russians. The eastern Soviet citizens — those with yellow skin and almond-shaped eyes — are considered to be at the bottom of society. They think of blacks as even worse.",
        "The reality of racism contrasts with the picture of social perfection painted by the authorities. It is maddening that Russians pride themselves on being free of racial prejudice. It is difficult for them to understand how thoroughly bigoted they are.",
      ],
      "uk.paragraphs": [
        "Усі неросіяни вважаються неповноцінними. За неофіційною шкалою неповноцінности вірмени, грузини та українці є трохи прийнятнішими за інших неросіян. Радянські громадяни зі Сходу — з жовтою шкірою і мигдалеподібними очима —вважаються низами суспільства. Ставлення ж до чорношкірих іще гірше.",
        "Наявність расизму суперечить картині соціальної досконалости, змальованій владою. Те, як росіяни пишаються цілковитою відсутністю расових упереджень, просто нестерпно. Їм важко второпати, наскільки вони упереджені.",
      ],
      "pic": "pics/mapofrussia.png",
      "uk.pic": "pics/mapamoskovytska.png",
    },
     {
      "author": "custine",
      "bubble": "The Russian people become incapable of anything except the conquest of the world.",
      "uk.bubble": "Російський народ має втратити здатність до всього, окрім завоювання світу.",
      "paragraphs": [
        "Russian social organization results in a fever of envy so violent, a stretch of mind towards ambition so constant, that the Russian people become incapable of anything except the conquest of the world.",
      ],
      "uk.paragraphs": [
        "З російського суспільного устрою виникає настільки потужна лихоманка заздрості, настільки нездоланний свербіж честолюбства, що російський народ має втратити здатність до всього, окрім завоювання світу.",
      ],
    },
 {
      "author": "custine",
      "bubble": " If passions calm in the West, the greedy hope of the conquering Russians will become a chimera.",
      "uk.bubble": "Якщо на Заході вщухнуть пристрасті, то жадібні завойовницькі сподівання росіян не втіляться в реальності",
      "paragraphs": [
        "The future — that brilliant future dreamt of by the Russians — does not depend upon them. They have no ideas of their own and the fate of this nation of imitators will be decided by people whose ideas are their own. If passions calm in the West, if union will be established between the governments and their subjects, the greedy hope of the conquering Russians will become a chimera.",
      ],
      "uk.paragraphs": [
        "Майбуття, яке мріється росіянам настільки блискучим для їхньої країни, від них самих не залежить. У них немає своїх ідей, тож доля цього народу наслідувачів буда вирішуватися там, де у народів є власні ідеї. Якщо на Заході вщухнуть пристрасті, якщо між урядами та підданими встановиться союз, то жадібні завойовницькі сподівання росіян не втіляться в реальності.",
      ],
      "pic": "pics/hungary.png",
      "uk.pic": "pics/hungaryuk.png",
    },
     {
      "author": "robinson",
      "bubble": "Soviet communist party members believe it is their country’s destiny to dominate the world.",
      "uk.bubble": "Члени радянської комуністичної партії переконані в тому, що доля їхньої країни — домінувати над світом.",
      "paragraphs": [
        "I am burdened by my knowledge that Soviet communist party members believe it is their country’s destiny to dominate the world. The message is everywhere. It is drummed into the heads of party members five-days-a-week, at meetings after work which they must attend, and which non-party members who want to gain favor from the officials in charge of their jobs, should attend.",
        "Children are influenced from an early age. They learn in school, in meetings of the Young Pioneers, and as members of the Komsomol, that it is Soviet destiny to defeat America and dominate the world.",
        "In the absence of any competing point of view, the indoctrination works. I can say without fear of contradiction, after living in the Soviet Union for forty-four years, that it is as natural for a Soviet citizen to believe that they should dominate the world as it is for an American to believe that the democratic system offers the best way of life.",
        "The Russian people are willing to endure almost any sacrifice to achieve this goal. They boast about their ability to tighten their belts and do whatever is necessary to bring down America.",
      ],
      "uk.paragraphs": [
        "Мене пригнічує усвідомлення того, що члени радянської комуністичної партії переконані в тому, що доля їхньої країни — домінувати над світом. Ця ідея повсюди. Її вбивають у голови партійців п’ять днів на тиждень — на післяробочих зібраннях з обов’язковою присутністю. І бажаною присутністю безпартійних, які намагаються здобути прихильність керівництва.",
        "На дітей впливають із раннього віку. У школі, на піонерських зборах і як члени комсомолу вони засвоюють, що призначення Радянського Союзу полягає в тому, аби перемогти Америку та панувати над світом.",
        "Завдяки відсутности будь-якої інакшої точки зору ця доктрина має успіх. Проживши в Радянському Союзі сорок чотири роки, я можу без жодних сумнівів заявити, що радянському громадянининові так само природно вірити, що він йому належить домінувати над світом, як американцеві вірити, що найкраще живеться за демократичної системи.",
        "Російський народ готовий іти на будь-які жертви заради цієї мети. Вони вихваляються своєю здатністю затягнути паски та зробити все необхідне, аби знищити Америку.",
      ],
    },
      {
      "author": "robinson",
      "bubble": "Because the Russians pride themselves on being free of prejudice, their racism is more dangerous than any I encountered in the United States.",
      "uk.bubble": "Оскільки росіяни пишаються відсутністю будь-яких упереджень, їхній расизм страшніший за будь-який, з яким я стикався в Сполучених Штатах.",
      "paragraphs": [
        "I could never, ever get used to the racism in the Soviet Union. It continually tested my patience and assaulted my sense of self-worth. Because the Russians pride themselves on being free of prejudice, their racism is more dangerous than any I encountered in the United States as a young man.",
        "I rarely met a Russian who thought blacks — or for that matter Orientals or any non-whites — were equal to him. Trying to deal with their prejudice was like trying to catch a phantom. I could feel their racism singeing my flesh, but how do you deal with something that officially doesn’t exist? I was the target of racism even though I had gained national recognition as a mechanical engineer by inventing machinery that dramatically increased industrial productivity.",
      ],
      "uk.paragraphs": [
        "Я ніколи, ніколи не міг звикнутися з расизмом у Радянському Союзі. Расизм безупинно випробовував моє терпіння і нівечив моє почуття власної гідности. Оскільки росіяни пишаються відсутністю будь-яких упереджень, їхній расизм страшніший за будь-який, з яким я стикався в Сполучених Штатах у молодости.",
        "Я рідко натрапляв на росіянина, який би ставився до чорношкірих — або жителів сходу, чи до будь-кого не з білою шкірою — як до рівного йому. Будь-які спроби побороти ці упередження були все одно що полювати на привида. Я відчував, як їхній расизм обпікає мені плоть, але як боротися з тим, чого офіційно не існує? Я був мішенню для расизму навіть попри те, що здобув національне визнання в якости інженера-механіка, винайшовши механізми, які істотно покращили промислове виробництво.",
      ],
       "pic": "pics/ruzkimarsh.jpg",
    },
     {
      "author": "custine",
      "bubble": "In Russia, at that period, everything was sacrificed to the future.",
      "uk.bubble": "У тогочасній росії все приносилося у жертву заради майбутнього.",
      "paragraphs": [
        "I was led to the sleeping apartment of Peter the great, Emperor of all the Russias. A carpenter of our days would not house his scholar in such a place.",
        "This glorious asceticism illustrates the epoch and the country as much as the man. In Russia, at that period, everything was sacrificed to the future: people were building palaces of their yet unborn masters; founders of those magnificent structures, not experiencing themselves the wants of luxury, were content to be providers for the future civilization, and took pride in preparing fitting residences for the unknown descendants.",
        "There is certainly a greatness of mind in this care. The obsession which the living have placed in the glory of their future generations has something about it which is noble and original. It is a disinterested and poetical sentiment — far greater than the respect to the ancestors, which men and nations are usually accustomed to.",
      ],
      "uk.paragraphs": [
        "Мені показали спальню петра великого, імператора всія русі: сучасний тесляр посоромився б поселити в ній свого учня.",
        "Ця вражаюча скромність надає нам змогу зрозуміти як епоху та країну, так і людей. У тогочасній росії все приносилося у жертву заради майбутнього: ті, для кого будувалися чудові палаци, ще не народилися на світ, а ті, хто їх будували, не мали жодної потреби в розкоші й, вдовольняючись роллю просвітників, пишалися можливістю підготувати палати, гідні своїх невідомих нащадків.",
        "Безперечно, в бажанні народу і його вождя зміцнити могутність і навіть потішити марнославство прийдешніх поколінь проявляється чимала велич душі; така віра у славу онуків благородна і своєрідна. Це почуття безкорисливе та поетичне, і значно перевершує почуття звичайних людей і націй, які вшановують не нащадків, а предків.",
      ],
    },
     {
      "author": "steinbeck",
      "bubble": "In Russia it is always the future that is thought of.",
      "uk.bubble": "У Росії передусім дбаються про майбутнє.",
      "paragraphs": [
        "In Russia it is always the future that is thought of. It is the crops next year, it is the comfort that will come in ten years, it is the clothes that will be made very soon. If ever a people took its energy from hope, it is the Russian people.",
      ],
      "uk.paragraphs": [
        "У Росії передусім дбаються про майбутнє. Це чи то врожаї наступного року, чи комфорт, якого буде досягнуто за десять років, чи одяг, який пошиють зовсім скоро. Якщо існує такий народ, який живиться надією, то це — російський.",
      ],
    },
     {
      "author": "custine",
      "bubble": "To degrade the tyrant into the hypocrite is a revenge, which comforts the victim.",
      "uk.bubble": "Жертва, яка змусила тирана лицемірити, почувається наче помстилася",
      "paragraphs": [
        "The lies of the despot, however palpable, are always flattering to the slave. The Russians, who bear so much, would bear no tyranny if the tyrant did not carefully act as though he believed they are obeying him out of good will. Human dignity, sinking in the gulf of absolute government, grabs the smallest branch within reach that may keep it afloat. Human nature will bear insults and mistreatment; but it will not bear to be told in direct terms that it is insulted and mistreated. When outraged by actions, it takes refuge in words. And lying is so humiliating, that to degrade the tyrant into the hypocrite is a revenge, which comforts the victim.",
      ],
      "uk.paragraphs": [
        "Брехня деспота, хоч би якою грубою вона була завжди лестить рабові. Росіяни, які покірно витримують стільки поневірянь, не стерпіли б тиранії, якби тиран не робив смиренний вигляд і не прикидався, що вважає, ніби вони коряться йому з доброї волі. Людська гідність, зневажена абсолютною монархією, хапається, як за соломинку, за будь-яку дрібницю: рід людський згоден терпіти зневагу і наругу, але не згоден, щоб йому чітко і ясно давали зрозуміти, що його зневажають і з нього знущаються. Ображені вчинками, люди ховаються за словами. Брехня настільки принизлива, що жертва, яка змусила тирана лицемірити, почувається наче помстилася",
      ],
      "pic": "pics/poternet.jpg",
    },
     {
      "author": "custine",
      "bubble": "Russia does not know today if the minister who governed her yesterday exists.",
      "uk.bubble": "І у Росії сьогодняшній вже немов і не існувало ніколи тієї людини, що лише вчора нею управляла.",
      "paragraphs": [
        "In Russia, on the day a minister falls from favour, his friends become deaf and blind. A man is, as it were, buried the moment he appears to be disgraced. Russia does not know today if the minister who governed her yesterday exists.",
      ],
      "uk.paragraphs": [
        "У Росії варто міністру втратити посаду, як його друзі негайно глухнуть і сліпнуть. Потрапити у немилість означає поховати себе заживо. І у Росії сьогодняшній вже немов і не існувало ніколи тієї людини, що лише вчора нею управляла.",
      ],
    },
     {
      "author": "steinbeck",
      "bubble": "Trotsky, as far as Russian history is concerned, has ceased to exist, and in fact never did exist.",
      "uk.bubble": "Для сучасної російської історії Троцького більше не існує, ба більше, його і не було.",
      "paragraphs": [
        "In the whole Lenin museum there is not one picture of Trotsky. Trotsky, as far as Russian history is concerned, has ceased to exist, and in fact never did exist. This is a kind of historical approach which we cannot understand. This is history as we wish it might have been rather than as it was.",
        "For there is no doubt that Trotsky exerted a great historical effect on the Russian Revolution. There is also no doubt that his removal and his banishment were of great historical importance.",
        "But to the young Russians he never existed. To the children who go into the Lenin Museum and see the history of the Revolution there is no Trotsky, good or bad.",
      ],
      "uk.paragraphs": [
        "У музеї Леніна немає жодного портрету Троцького. Для сучасної російської історії Троцького більше не існує, ба більше, його і не було. Подібне поводження з історією нам незрозуміле. Це історія, яку хотілося б мати, замість тієї, якою вона була насправді.",
        "Бо немає сумніву, що Троцький відіграв неабияку історичну роль у російській революції. Немає також сумніву, що його усунення та забуття мають вагоме історичне значення.",
        "Але для молодих росіян він ніколи не існував. Для дітей, які відвідують музей Леніна і споглядають історію революції, не існує Троцького, ані хорошого, ані поганого.",
      ],
    },
     {
      "author": "robinson",
      "bubble": "To the hundreds of thousands of people massed in Red Square, Stalin was their modern-day czar.",
      "uk.bubble": "Для сотень тисяч людей, які зібралися на Червоній площі, Сталін був їхнім сучасним царем",
      "paragraphs": [
        "As the crowd began chanting for Joseph Stalin to appear, I sensed they were acting just like their forefathers had in decades and centuries past, when they came to shower affection upon and seek reassurance from the czars. To the hundreds of thousands of people massed in Red Square, Stalin was not viewed as the head of the Communist party. Instead, he was the leading Russian potentate. He was their modern-day czar who, instead of defending the crown defended the hammer and sickle.",
      ],
      "uk.paragraphs": [
        "Коли натовп людей почав скандувати, щоби до них вийшов Йосип Сталін, у мене склалося враження, що вони поводилися так само, як їхні пращури, які протягом десятиріч і сторіч засвідчували пошану царям і прагнули їхньої милости. Для сотень тисяч людей, які зібралися на Червоній площі, Сталін був аж ніяк не головою Комуністичної партії. Натомість він був панівним російським монархом. Він був їхнім сучасним царем, який замість корони боронив серп і молот.",
      ],
      "pic": "pics/forempireen.jpg",
      "uk.pic": "pics/forempireuk.jpg",
    },
     {
      "author": "custine",
      "bubble": "They obey violence, which they take for power.",
      "uk.bubble": "Народ підкоряється безжальній владі, бо сприймає злобу за силу.",
      "paragraphs": [
        "Kindness is called a weakness among the people hardened by terror: cruelty makes them bend the knee, pardon would cause them to lift the head. They can be forced, but not persuaded. Incapable of honor, they can yet be audacious. They revolt against kindness, but they obey violence, which they take for power.",
      ],
      "uk.paragraphs": [
        "У народу, загартованого постійним страхом, милосердя вважається слабкістю. З таким народом можна впоратися лише залякуваннями. Невблаганна суворість ставить його на коліна, м'якість же, навпаки, надала б йому змогу піднести голову. Його не можна переконати, але можна змусити коритися, він не вміє бути гордим, але може стати зухвалим. Він бунтує проти поблажливої влади та підкоряється безжальній, бо сприймає злобу за силу.",
      ],
    },
     {
      "author": "robinson",
      "bubble": "There are two things which the Soviets understand thoroughly: power and reciprocity.",
      "uk.bubble": "Є дві речі, які в Союзі добре розуміють: сила і взаємовигода.",
      "paragraphs": [
        "There are two things which the Soviets understand thoroughly: power and reciprocity. If we have power and demonstrate the courage to use it, they will respect us. If we have the power but lack the will to use it or if we allow ourselves to become weaker and weaker — the Soviets will use their power against us.",
      ],
      "uk.paragraphs": [
        "Є дві речі, які в Союзі добре розуміють: сила і взаємовигода. Якщо у нас є сила, і ми демонструємо готовність її застосувати, нас поважатимуть. Якщо у нас є сила, але бракує рішучости нею скористатися, або якщо ми дозволимо собі ставати все слабшими і слабшими — совєти застосують свою силу проти нас.",
      ],
       "pic": "pics/pathetic.jpg",
    },
     {
      "author": "robinson",
      "bubble": "From kindergarten to old age everyone is exposed to a constant barrage of political indoctrination.",
      "uk.bubble": "Від дитсадка і до похилого віку кожен перебуває під нескінченним впливом політичного виховання.",
      "paragraphs": [
        "Since the October Revolution of 1917, millions of people have been educated through the Soviet system. From kindergarten to old age everyone is exposed to a constant barrage of political indoctrination. Soviet students soon learn that they are not allowed to think freely or question dogma without running into trouble with the authorities.",
        "As a result, their viewpoints remain narrow. Whether they are in service to the advancement of the state is of primary importance. The point was to dedicate one’s life to helping Mother Russia achieve what was taught as its so-called sacred mission — to achieve world domination even if achieving the goal took a hundred years. Developing individual intellectual potential is not a priority.",
        "Soviet education does not stress the development of a well-rounded, developed mind. For this reason, the Soviet intelligentsia is different from the intellectual elite in the West. This is an important point which the West needs to understand.",
        "The peasant students entering the institute, usually in their late twenties, still had a village frame of mind at graduation time. They were trained to perform a specific function, and they knew that by doing that well they were guaranteed housing, food, and a limited medical care plan. Why waste energy learning more about the world? It did not make any sense. They were by and large guided by the Russian street maxim: “Make the most of the safe situation you are in. To break with the norm is to invite trouble, or even death”.",
        "The great majority of Russians were conditioned to accept their lot in life and be thankful for what they had.",
      ],
      "uk.paragraphs": [
        "Після Жовтневої революції 1917 року мільйони людей здобули освіту за радянською системою. Від дитсадка і до похилого віку кожен перебуває під нескінченним впливом політичного виховання. Водночас радянські студенти швидко усвідомлюють, що надто вільні роздуми або сумніви щодо будь-яких догм призводять до проблем із владою.",
        "Унаслідок цього їхні кути зору звужуються. Передусім вони вчаться сприяти розвитку держави. Мета навчання полягає в тому, щоб учні присвятили своє життя матінці-Росії, допомагаючи їй виконати свою так звану священну місію, а саме — досягти світового панування, навіть якщо шлях до цієї мети триватиме сто років. Розвиток розумового потенціалу особистости не є пріоритетом.",
        "Радянська освіта не має на меті розвиток всебічно розвиненого інтелекту. Через це радянська інтелігенція відрізняється від інтелектуальної еліти Заходу. Це важлива відмінність, яку Заходу необхідно усвідомити.",
        "Студенти-селяни, які вступали до інституту здебільшого у віці за двадцять, після випуску все ще зберігали кріпацьку свідомість. Їх було навчено виконувати певні функції, і всі вони знали, що за сумлінного виконання цих функцій їм гарантовано житло, їжу та медичне обслуговування. То нащо витрачати енергію на пізнання навколишнього світу? У цьому не було жодного значення. Усі дотримувалися російської вуличної мудрости: витягти з життя максимум, поки маєш на те змогу. Порушуючи ж заведені норми, накличеш на себе біду або навіть смерть.",
        "Переважну більшість росіян привчили коритися долі і бути вдячними за те, що вони мають.",
      ],
      "pic": "pics/zetgen.jpg",
    },
     {
      "author": "bahrianyi",
      "bubble": "The goal of Bolshevism is to create a unified totalitarian red empire with a single administration, language, culture and ideology. Therefore, the slightest manifestation of the will of any of the ‘equal’ nations in the USSR (except Russia) is suppressed with terrible moral and physical terror.",
      "uk.bubble": "Найменший прояв власної волі будь-якої з «рівноправних» націй в СРСР (окрім Росії) здушується страшним моральним і фізичним терором. Тим терором російський червоний фашизм (більшовизм) намагається перетворити 100 національностей в т.зв. «єдиний радянський народ», цебто фактично в російський народ.",
      "paragraphs": [
        "Love for one’s nation, that is, national patriotism, is the most serious crime in the USSR. This crime is called ‘local nationalism’ in the Bolshevik language, the language of red Moscow fascism.",
        "Why is this considered the most serious crime? The USSR is known to be a federation of equal republics. According to the ‘Stalinist constitution’, everyone has the right to national freedom and even the right to secede from the USSR. Meaning that each representative of each nation allegedly has the right to express patriotism and love for their people.",
        "However, this is purely theoretical.",
        "In practice, if any republic attempts to leave the federation, it will be crushed by fire and iron by its ‘equal’ ally, Bolshevik Russia. And it would have been held by intimidation and terror in that free union.",
        "The goal of Bolshevism is to create a unified totalitarian red empire with a single administration, language, culture and ideology. Therefore, the slightest manifestation of the will of any of the ‘equal’ nations in the USSR (except Russia) is suppressed with terrible moral and physical terror.",
        "With this terror, Russian red fascism (Bolshevism) is trying to turn 100 nationalities into the so-called ‘single Soviet people’. That is, in reality, into the Russian people.",
        "Hence, ‘local’ patriotism, desire for national freedom and independence from any of the representatives of those 100 nationalities is considered the most serious crime and is so severely punished.",
      ],
      "uk.paragraphs": [
        "Любов до Вітчизни, до свого народу, цебто національний патріотизм в СРСР є найтяжчим злочином. Злочин цей зветься на більшовицькій мові — на мові червоного московського фашизму — «місцевим націоналізмом».",
        "Чому це вважається за найтяжчий злочин? СРСР, як відомо, є федерацією рівноправних республік. Рівноправних народів, що по «сталінській конституції» мають право на національну свободу аж до відокремлення від СРСР. А значить, кожний представник від кожної нації нібито має право на свій патріотизм і на любов до свого народу.",
        "Одначе це лише в теорії.",
        "А насправді, коли б якась республіка захотіла вийти з федерації, вона була б задавлена вогнем і залізом своїм «рівноправним» союзником — більшовицькою Росією. І була б весь час наставлена і утримувана терором у тім вільнім союзі.",
        "Більшовизмові залежить на створенні єдиної тоталітарної червоної імперії з єдиною адміністрацією, єдиною мовою, єдиною культурою, єдиною ідеологією й політикою.",
        "Тому найменший прояв власної волі будь-якої з «рівноправних» націй в СРСР (окрім Росії) здушується страшним моральним і фізичним терором.",
        "Тим терором російський червоний фашизм (більшовизм) намагається перетворити 100 національностей в т.зв. «єдиний радянський народ», цебто фактично в російський народ.",
        "Ось тому патріотизм «місцевий», цебто патріотизм кожного з представників отих 100 національностей — прагнення до національної свободи й незалежності — вважається за найтяжчий злочин й так тяжко карається.",
      ],
     },
      {
      "author": "koestler",
      "bubble": "I learnt to classify automatically everything that shocked me as the ‘heritage of the past’ and everything I liked as the ‘seeds of the future’.",
      "uk.bubble": "Я навчився автоматично зараховувати все, що мене обурювало, до «спадщини минулого», а все хороше іменувати «насінням майбутнього»",
      "paragraphs": [
        "... And yet I remained a convinced Communist. I learnt to classify automatically everything that shocked me as the ‘heritage of the past’ and everything I liked as the ‘seeds of the future’. By setting up this automatic sorting machine in his mind, it was still possible in 1932 for a European to live in Russia and yet to remain a Communist. All foreign comrades I knew, and also the more mentally alert among the Russians, had that automatic sorting machine in their heads.",
        "Living standards were low, but under the Czarist régime they had been even lower. The working classes in the capitalist countries were better off than in the Soviet Union, but that was a static comparison, for here the level was steadily rising, there steadily falling. At the end of the second Five Year Plan the two levels would be equalised; until that time all comparisons were misleading, and bad for the Soviet People’s morale.",
        "Accordingly, I not only accepted the famine as inevitable, but also the necessity of the ban on foreign travel, foreign newspapers and books, and the dissemination of a grotesquely distorted picture of life in the capitalist world.",
        "At first I was shocked when after a lecture I was asked questions like these: ‘What is the average number per day of French working-class families starving to death (a) in rural areas, (b) in the towns?”, always painstakingly formulated in neo-russian Stalinese. After a while I found them quite natural. There was always a small element of truth in them — this had, of course, been exaggerated and simplified according to the accepted techniques of propaganda; but propaganda was indispensable for the survival of the Soviet Union, surrounded by a hostile world.",
      ],
      "uk.paragraphs": [
        "... І все ж я залишався переконаним комуністом. Я навчився автоматично зараховувати все, що мене обурювало, до «спадщини минулого», а все хороше іменувати «насінням майбутнього». Налаштувавши у своєму мозку цей автоматичний сортувальний пристрій, європеєць у такий спосіб усе ще міг бути комуністом, мешкаючи в Росії 1932 року. Усі мої товариші-іноземці, так само як і найбільш свідомі росіяни мали у голові цей автоматичний сортувальний пристрій.",
        "Рівень життя був низьким, але за царського режиму він був ще нижчим. Робітничи класи у капіталістичних країнах перебували в кращому становищі, ніж у Радянському Союзі, однак це було статичним порівнянням, оскільки тут рівень неухильно зростав, а там неухильно знижувався. Наприкінці другої п'ятирічки ці два рівні зрівняються; а доти будь-які порівняння лише вводили радянських людей в оману і шкодили моральному духу.",
        "Тож голодомор я вбачав як щось неминуче, так само як і необхідність заборони закордонних поїздок, іноземних газет і книжок, а також розповсюдження гротескно спотвореної картини життя в капіталістичному світі.",
        "Спочатку мене приголомшували запитання на кшталт: «Яка середня кількість сімей робітничого класу Франції вмирає від голоду на день (а) у сільській місцевості, (б) у містах?», завжди старанно сформульовані «новоросійською сталінською». Однак згодом вони здавалися мені цілком природними. Вони завжди містили крихітну частку правди — безумовно, перебільшену та спрощену відповідно до загальноприйнятих методів пропаганди; але пропаганда була необхідною для виживання Радянського Союзу, оточеного ворожим світом.",
      ],
    },
     {
      "author": "applebaum",
      "bubble": "Moscow’s paranoia about the counter-revolutionary potential of Ukraine continued into the 1970s and 1980s.",
      "uk.bubble": "Московська паранойя щодо контрреволюційного потенціялу України продовжувалася у 1970-1980-х роках",
      "paragraphs": [
        "Moscow’s paranoia about the counter-revolutionary potential of Ukraine continued after the Second World War, and into the 1970s and 1980s. It was taught to every successive generation of secret policemen, from the OGPU to the NKVD to the KGB, as well as every successive generation of party leaders. Perhaps it even helped mould the thinking of the post-Soviet elite, long after the USSR ceased to exist.",
      ],
      "uk.paragraphs": [
        "Московська паранойя щодо контрреволюційного потенціялу України повернулася після Другої світової війни, і продовжувалася у 1970-1980-х роках. Неї навчали кожне наступне покоління спецслужб, від ОДПУ до НКВС і КДБ, а також кожне наступне покоління партійних лідерів. Це, найімовірніше, впливало на формування поглядів пострадянської еліти протягом тривалого часу навіть після розпаду СРСР.",
      ],
    },
     {
      "author": "koestler",
      "bubble": "The necessary intimidation of the masses to preserve them from error.",
      "uk.bubble": "Таке необхідне залякування мас, щоб уберегти їх від помилок.",
      "paragraphs": [
        "The necessary propaganda-lie; the necessary intimidation of the masses to preserve them from error; the necessary liquidation of opposition groups and hostile social classes; the necessary sacrifice of a whole generation in the interest of the next — it may all sound revolting, and yet it was quite easy to accept while one was rolling along the single track of faith.",
        "My faith in communism no longer was the naive faith of a year before, when I had got into the train in Berlin expecting that it would take me straight to Utopia. It had become a rather wistful, rather esoteric faith, but all the more elastic. I no longer believed in Communism because of the example, but in spite of it. And a faith that is held ‘in spite of’ is always more resilient and less open to disillusion than one that is based on a ‘because’.",
      ],
      "uk.paragraphs": [
        "Така необхідна брехня пропаганди; таке необхідне залякування мас, щоб уберегти їх від помилок; така необхідна ліквідація опозиції та ворожих соціальних класів; така необхідна жертва геть усього покоління в інтересах наступного — усе це здатне викликати відразу, а проте з цим було легко погоджуватися, прямучи хибним шляхом своєї віри.",
        "Моя віра в комунізм уже не була тією наївною минулорічною вірою, коли я сідав на потяг у Берліні, сподіваючись, що він навпростець повезе мене до Утопії. Вона стала понурою, химерною вірою, і разом із цим — еластичнішою. Я вірив у комунізм не завдяки, а всупереч йому. А віра, якої тримаються «попри будь-що», завжди стійкіша до розчарувань, аніж та, якої тримаються «внаслідок» чогось.",
      ],
      "pic": "pics/lebedeven.png",
      "uk.pic": "pics/lebedevuk.png",
    },
     {
      "author": "clarke",
      "bubble": "The Malo-Russians offered a picture of contentment and peace not often found within Russian territories",
      "uk.bubble": "Малоруси випромінюють радість і спокій, дуже рідкісні в межах російських територій.",
      "paragraphs": [
        "If happiness could be found under the Russian government, it might be said to dwell in Dobrinka — a peaceable and pleasant spot, full of neat little white cottages, tenanted by a healthy and apparently joyful society. They live in the greatest tranquillity, removed from all the spies, tax gatherers, police officers, and other despots of the country.",
        "We were received into one of the courtyards with a hearty welcome and with smiles, very different from the lowering brows and contracted suspicious eyes, to which we had been so often accustomed [in Russia].",
        "The Malo-Russians, with their numerous families, were seated on the ground in circles before their neat little habitations, eating their supper; and, being all happy and merry together, offered a picture of contentment and peace not often found within Russian territories.",
      ],
      "uk.paragraphs": [
        "Якби щастя було знайдено під російським урядом, тоді б його резиденція була в Добринці, де живуть малоросіяни — у спокійному та приємному місці, де маленькі білі хатки замешкані здоровою і, очевидно, злагодною спільнотою, чиї члени живуть у тихому спокої, позбавлені шпигунів, збирачів податків, поліції та інших дрібних деспотів країни.",
        "Ми були прийняті в одному з їхніх дворів із сердечним привітанням та усмішкою, що сильно відрізнялося від насуплених брів і підозрілих поглядів, до яких ми так звикли [в росії].",
        "Малоруси з численними їхніми родинами сидять на землі колами, вечеряють біля своїх акуратних хаток, і всі вони разом випромінюють радість і спокій, дуже рідкісні в межах російських територій.",
      ],
    },
     {
      "author": "custine",
      "bubble": "Russian injustices will shock the senses of the world.",
      "uk.bubble": "Весь світ здригнеться від побачених несправедливостей.",
      "paragraphs": [
        "When the sun of publicity shall rise upon Russia, how many injustices will it expose to view! — not only ancient ones, but those which are still enacted daily, will shock the senses of the world.",
      ],
      "uk.paragraphs": [
        "Коли над росією зійде сонце гласности, весь світ здригнеться від побачених несправедливостей — не тільки давніх, а й тих, що понині творяться щодня.",
      ],
    },
     {
      "author": "custine",
      "bubble": "That nation, essentially aggressive, compensates hardship by exercising a tyranny over other nations.",
      "uk.bubble": "Та нація компенсує тяготи життя, підкорюючи та згнічуючи інші нації.",
      "paragraphs": [
        "In the heart of the Russian people ferments an inordinate ambition, one of those which could only possibly rise in the hearts of the oppressed, and could only find nourishment in the miseries of a whole nation. That nation, essentially aggressive, greedy under the influence of hardship, compensates with degrading submission — the design of exercising a tyranny over other nations. The glory and wealth, which it hopes for, make up for its disgrace.",
        "To purify himself from the wicked sacrifice of all public and personal liberty, the slave, upon his knees, dreams of the conquest of the world.",
      ],
      "uk.paragraphs": [
        "У серцях російських людей бродить страшна амбіція, одна з тих, яка може зростати лише у серцях поневолених й пригнічених людей, яка здатна живитися лише стражданнями цілої нації. Та нація, докорінно агресивна, жадібна внаслідок тягот життя, компенсує це все жорстоким гнобленням — а саме, підкорюючи та згнічуючи інші нації. А завойовницька слава та багатства, на які вона розраховує, компенсують їй безчестя.",
        "Аби очищитися від безбожної пожертви буд-яких своїх громадських і особистих свобод, раб, стоючи навколішки, мріє про те, як підкорить собі увесь світ.",
      ],
      "pic": "pics/russianwarsen.png",
      "uk.pic": "pics/russianwarsuk.png",
    },
  ];
